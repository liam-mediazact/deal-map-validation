const validationUtil = require('./src/validationUtil');
const voucher = require('./src/voucher');
const payment = require('./src/payment');

module.exports = {
  ...validationUtil,
  ...voucher,
  ...payment
};
