const yup = require('yup');

const stringSchema = yup.string().nullable();

const nonEmptyStringSchema = yup.string();

const idSchema = yup.number();

const nullableIdSchema = yup.number().nullable();

const authIdSchema = yup.string();

const dateSchema = yup.date();

const numberSchema = yup
  .number()
  .integer()
  .nullable();

const emailSchema = yup.string().email();

const idArraySchema = yup.array().of(idSchema);

const booleanSchema = yup.boolean().strict();

const validateSchema = (object, schema) => {
  const { error } = yup.validate(object, schema);
  if (error) throw Error(error);
};

module.exports = {
  stringSchema,
  nonEmptyStringSchema,
  authIdSchema,
  dateSchema,
  numberSchema,
  emailSchema,
  idArraySchema,
  validateSchema,
  idSchema,
  nullableIdSchema,
  booleanSchema
};
