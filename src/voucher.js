const yup = require('yup');
const {
  validateSchema,
  authIdSchema,
  idSchema,
  nullableIdSchema,
  stringSchema,
  numberSchema
} = require('./validationUtil');

const CreateVoucherSchema = yup.object().shape({
  type: numberSchema.required(),
  number: stringSchema.required(),
  description: stringSchema.required(),
  date: stringSchema.required(),
  amount: numberSchema,
  contactId: idSchema.required(),
  dueDate: stringSchema.required(),
  status: nullableIdSchema,
  currencyCode: stringSchema,
  currencyRate: numberSchema,
  currencyAmount: numberSchema,
  accountId: idSchema.required(),
  isIntegrated: numberSchema.required()
});

const EditVoucherSchema = yup.object().shape({
  voucherId: nullableIdSchema.required(),
  type: numberSchema.required(),
  number: stringSchema.required(),
  description: stringSchema,
  date: stringSchema.required(),
  amount: numberSchema,
  contactId: idSchema.required(),
  dueDate: stringSchema.required(),
  status: nullableIdSchema,
  currencyCode: stringSchema,
  currencyRate: numberSchema,
  currencyAmount: numberSchema,
  accountId: idSchema.required(),
  isIntegrated: numberSchema.required()
});

module.exports = {
  CreateVoucherSchema,
  EditVoucherSchema
};
