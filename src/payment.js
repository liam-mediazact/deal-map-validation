const yup = require('yup');
const {
  validateSchema,
  authIdSchema,
  idSchema,
  nullableIdSchema,
  stringSchema,
  numberSchema
} = require('./validationUtil');

const CreatePaymentSchema = yup.object().shape({
  contactId: nullableIdSchema.required(),
  paymentDate: stringSchema,
  depositDate: stringSchema,
  clearedDate: stringSchema,
  amount: numberSchema,
  description: stringSchema,
  status: nullableIdSchema.required(),
  type: numberSchema.required(),
  tranType: numberSchema.required(),
  isCleared: numberSchema.required(),
  accountId: idSchema.required(),
  isIntegrated: numberSchema.required()
});

module.exports = {
  CreatePaymentSchema
};
